const request = require('request');
exports.handler = async (event) => {
    let status_code;
    try {
        let activity_details;
            if(event.hasOwnProperty('Current')){
                activity_details = {
                    ...event.Data,
                    ...event.Current
                };
                
                activity_details['ProspectActivityId'] = event.ProspectActivityId;
                activity_details['RelatedProspectId'] = event.RelatedProspectId;
                activity_details['ActivityEvent'] = event.ActivityEvent;
                activity_details['ActivityEventName'] = event.ActivityEventName;
                activity_details['CreatedBy'] = event.CreatedBy;
                activity_details['CreatedOn'] = event.CreatedOn;
            }else{
                activity_details = event['After'];
            }
            let epfo_details = await getEmploymentVerification(activity_details);
            epfo_details = JSON.parse(epfo_details);
            
            
            let pdf_link = "";
            if(epfo_details["status-code"] == 101){
                pdf_link = epfo_details.result.pdfLink;
            }
            if(pdf_link){
                let response = await postData(pdf_link, activity_details);
                status_code = 200;
                return getResponse(status_code, response);
                
            }else {
                status_code = 102;
                return getResponse(status_code, "No Records Found");
            }
            
        
    }catch(e){
        console.log(e);
        status_code = 500;
        return getResponse(status_code, e);
    }
    
    
};

function postData(pdf_link, activity_details){
    let payload = {
            "PDFLink": pdf_link,
            "ActivityData": activity_details
        };
    
    let options = {
        url: process.env.LSQ_URL,
        qs: {
            name: process.env.LAPP_NAME,
            stage: "Live"
        },
        body: JSON.stringify(payload),
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "x-api-key": process.env.API_KEY_LSQ
        }
    };

        
    return new Promise((resolve, reject) => {
        request(options, (error, response, body) => {
            if(error){
                reject(error);
            }else{
                if(response.statusCode == 200) resolve("Data Posted Successfully");
                else reject(response);
            }
        });
    });
}

function getResponse(status_code, body){
    let response = {
        statusCode: status_code,
        body: body
    };
    return response;
}

function getEmploymentVerification(activity_details){
    let name = JSON.parse(activity_details.mx_Custom_3)['mx_CustomObject_11'];

    let employerName = JSON.parse(activity_details.mx_Custom_3)['mx_CustomObject_7'];
    let phone = activity_details.Phone.slice(-10);
    let email = activity_details.mx_Secondary_Email_Address;

    let payload = {
        "employerName": employerName,
        "employeeName": name,
        "mobile": phone,
        "emailId": email,
        "pdf": true,
        "uans": []
    };

    let options = {
        url: process.env.KARZA_URL,
        body: JSON.stringify(payload),
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "x-karza-key": process.env.API_KEY_KARZA
        }
    };

    return new Promise((resolve, reject) => {
        request(options, (error, response, body) => {
            if(error){
                reject(error);
            }else{
                if(response.statusCode == 200) resolve(body);
                else reject(response);
            }
        });
    });
}